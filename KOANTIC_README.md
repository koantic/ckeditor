# Koantic CKEditor integration


## Build

    ./dev/builder/build.sh -s
    copy ./dev/builder/release/ckeditor to koantic_project/meteor/public/ckeditor

## Development

    sudo npm install -g http-server
    http-server -p 8000 -c-1

Edit /meteor/imports/client/form/components/HtmlEditor.tsx and replace

  tag.src = '/ckeditor/ckeditor.js';

by

  tag.src = 'http://127.0.0.1:8000/ckeditor.js';

## Modifications to CKEditor
- added the following plugins:
    - confighelper
        - for placeholder text
    - koancolor
        - connect CKEditor ot our color picker
    - letterspacing
- set the skin to flat
- modify the following builtin plugin: 
    - table and tabletools
        - use style for border, border color, border-top/left..., background color and cellpadding
    - tableresize
        - fix a bug when the parent scale the ckeditor in css

## Add a new plugin
- Add the plugin in the plugins folder
- add it to the ./dev/builder/build-config.js
- In Koantic, add it to meteor/imports/client/form/utils/ckeditorConfig.jsx