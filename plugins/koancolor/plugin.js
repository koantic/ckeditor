var ColorCommand = function(isBackground) {
	this.isBackground = isBackground;

	function isUnstylable(ele) {
		return (
			ele.getAttribute("contentEditable") == "false" ||
			ele.getAttribute("data-nostyle")
		);
	}
	if (isBackground) {
		this.styleDefinition = CKEDITOR.config.colorButton_backStyle;
		this.styleDefinition.childRule = function(element) {
			// It's better to apply background color as the innermost style. (#3599)
			// Except for "unstylable elements". (#6103)
			return isUnstylable(element);
		};
		this.contentTransformations = [
			[
				{
					// Transform span that specify background with color only to background-color.
					element: "span",
					left: function(element) {
						var tools = CKEDITOR.tools;
						if (
							element.name != "span" ||
							!element.styles ||
							!element.styles.background
						) {
							return false;
						}

						var background = tools.style.parse.background(
							element.styles.background
						);

						// We return true only if background specifies **only** color property, and there's only one background directive.
						return (
							background.color &&
							tools.objectKeys(background).length === 1
						);
					},
					right: function(element) {
						var style = new CKEDITOR.style(
								editor.config.colorButton_backStyle,
								{
									color: element.styles.background
								}
							),
							definition = style.getDefinition();

						// Align the output object with the template used in config.
						element.name = definition.element;
						element.styles = definition.styles;
						element.attributes = definition.attributes || {};

						return element;
					}
				}
			]
		];
	} else {
		this.styleDefinition = CKEDITOR.config.colorButton_foreStyle;
		this.styleDefinition.childRule = function(element) {
			// Fore color style must be applied inside links instead of around it. (#4772,#6908)
			return (
				!(element.is("a") || element.getElementsByTag("a").count()) ||
				isUnstylable(element)
			);
		};
		this.contentTransformations = [
			[
				{
					element: "font",
					check: "span{color}",
					left: function(element) {
						return !!element.attributes.color;
					},
					right: function(element) {
						element.name = "span";

						element.attributes.color &&
							(element.styles.color = element.attributes.color);
						delete element.attributes.color;
					}
				}
			]
		];
	}

	this.contextSensitive = true;
};

ColorCommand.prototype.exec = function(editor, param) {
	editor.focus();
	editor.fire("saveSnapshot");
	// Clean up any conflicting style within the range.
	editor.removeStyle(
		new CKEDITOR.style(this.styleDefinition, {
			color: "inherit"
		})
	);

	if (param.color) {
		editor.applyStyle(
			new CKEDITOR.style(this.styleDefinition, {
				color: param.color
			})
		);
		editor.fire("saveSnapshot");
	}
};

ColorCommand.prototype.refresh = function(editor) {
	var selection = editor.getSelection(),
		block = selection && selection.getStartElement(),
		color;

	// Find the closest block element.
	//block = path.block || path.blockLimit || editor.document.getBody();

	// The background color might be transparent. In that case, look up the color in the DOM tree.
	do {
		color =
			(block &&
				block.getComputedStyle(
					this.isBackground ? "background-color" : "color"
				)) ||
			"transparent";
	} while (
		this.isBackground &&
		color == "transparent" &&
		block &&
		(block = block.getParent())
	);
	// The box should never be transparent.
	if (!color || color == "transparent") color = "#ffffff";
	this.setState(color);
};

CKEDITOR.plugins.add("koancolor", {
	// jscs:disable maximumLineLength
	lang: "en,fr", // %REMOVE_LINE_CORE%
	// jscs:enable maximumLineLength
	icons: "bgcolor,textcolor", // %REMOVE_LINE_CORE%
	hidpi: true, // %REMOVE_LINE_CORE%
	init: function(editor) {
		var config = editor.config,
			lang = editor.lang.koancolor;

		editor.addCommand("textColor", new ColorCommand(false));
		editor.addCommand("backColor", new ColorCommand(true));

		if (!CKEDITOR.env.hc) {
			addButton(
				"TextColor",
				"fore",
				lang.textColorTitle,
				10,
				"textColor"
			);

			addButton("BGColor", "back", lang.bgColorTitle, 20, "backColor");
		}

		function addButton(name, type, title, order, command) {
			var style = new CKEDITOR.style(
				config["colorButton_" + type + "Style"]
			);

			var activeColorPicker;

			var updateColor = function(color, callbackReason) {
				if (callbackReason !== "updated") {
					this.setState(CKEDITOR.TRISTATE_OFF);
					activeColorPicker = undefined;
				}

				if (callbackReason === "chose") {
					editor.execCommand(command, {color: color});
				}
			};

			var clickFn = function() {
				activeColorPicker = this._.id;
        this.setState(CKEDITOR.TRISTATE_ON);
        
        var currentColor = editor.getCommand(command).state;
        
				window.toggleColorPicker(
					this._.id,
					currentColor,
					updateColor.bind(this)
				);
			};

			editor.ui.addButton(name, {
				label: title,
				title: title,
				modes: { wysiwyg: 1 },
				editorFocus: 0,
				toolbar: "colors," + order,
				allowedContent: style,
				requiredContent: style,
				click: clickFn,

				refresh: function() {
					if (!editor.activeFilter.check(style))
						this.setState(CKEDITOR.TRISTATE_DISABLED);
				}
			});

			function closeColorPicker() {
				if (activeColorPicker) {
					window.closeColorPicker(activeColorPicker);
					activeColorPicker = undefined;
				}
			}
			editor.on("destroy", closeColorPicker);
		}

		//this is needed for table color
		editor.plugins.colordialog = {};
		editor.getColorFromDialog = function(callback, scope) {
			var updateColor = function(color) {
				callback.call(scope, color);
			};

			var initialColor = "";
			if (scope.id === "bgColorChoose") {
				initialColor = scope
					.getDialog()
					.getContentElement("info", "bgColor")
					.getValue();
			} else if (scope.id === "borderColorChoose") {
				initialColor = scope
					.getDialog()
					.getContentElement("info", "borderColor")
					.getValue();
			} else if (scope.id === "txtBorderColorChoose") {
				initialColor = scope
					.getDialog()
					.getContentElement("info", "txtBorderColor")
					.getValue();
			} else if (scope.id === "txtBgColor") {
				initialColor = scope
					.getDialog()
					.getContentElement("info", "txtBgColorChoose")
					.getValue();
			}

			window.toggleColorPicker(
				scope.domId,
				initialColor,
				updateColor.bind(this)
			);
		};
	}
});

/**
 * Stores the style definition that applies the text foreground color.
 *
 * Read more in the [documentation](#!/guide/dev_colorbutton)
 * and see the [SDK sample](http://sdk.ckeditor.com/samples/colorbutton.html).
 *
 *        // This is actually the default value.
 *        config.colorButton_foreStyle = {
 *			element: 'span',
 *			styles: { color: '#(color)' }
 *		};
 *
 * @cfg [colorButton_foreStyle=see source]
 * @member CKEDITOR.config
 */
CKEDITOR.config.colorButton_foreStyle = {
	element: "span",
	styles: { color: "#(color)" },
	overrides: [
		{
			element: "font",
			attributes: { color: null }
		}
	]
};

/**
 * Stores the style definition that applies the text background color.
 *
 * Read more in the [documentation](#!/guide/dev_colorbutton)
 * and see the [SDK sample](http://sdk.ckeditor.com/samples/colorbutton.html).
 *
 *        // This is actually the default value.
 *        config.colorButton_backStyle = {
 *			element: 'span',
 *			styles: { 'background-color': '#(color)' }
 *		};
 *
 * @cfg [colorButton_backStyle=see source]
 * @member CKEDITOR.config
 */
CKEDITOR.config.colorButton_backStyle = {
	element: "span",
	styles: { "background-color": "#(color)" }
};
